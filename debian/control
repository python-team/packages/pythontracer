Source: pythontracer
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: TANIGUCHI Takaki <takaki@debian.org>
Build-Depends: debhelper (>= 7.0.50~),
 python-all-dev, python-setuptools, dh-python
Standards-Version: 3.9.1
Homepage: http://code.google.com/p/pythontracer/
Vcs-Git: https://salsa.debian.org/python-team/packages/pythontracer.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/pythontracer

Package: pythontracer
Architecture: any
Depends: ${python:Depends},${shlibs:Depends}, ${misc:Depends}, python-gtk2
Description: Python programs' execution tracer and profiler
 Lets you see your Python program's execution as a tree of function
 invocations, each tree node exposing the real time, and CPU time (user/sys)
 of that call.
 .
 This project consists of two main components: A Python tracer that can run
 your Python programs (much like "cProfile" and friends). A Gtk+ based GUI
 that can show the trace results.
 .
 It uses a tiny auxiliary library written for it "graphfile" to allow
 append-only writing and reading static DAG's directly from file without
 reading it whole into memory at any stage.
